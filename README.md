# Scope

This project is part of the GitLab Group: https://gitlab.com/pck-info-issue

The project contains only a single enum `com.example.ColorEnum` and it uses the [EnumMapper](https://github.com/tmtron/enum-mapper) annotation processor.

When you build the project the annotation processor will create a file `com.example.ColorEnum_MapperFull.java`
in `build/generated/source/apt/main`. This java file will be compiled and it will **not** end up in the generated
`.jar` file (`build/libs/color-1.0-SNAPSHOT.jar`)

## Build

To build the project and generate the jar file:

* Windows: `gradlew.bat clean jar`
* Linux:   `gradlew clean jar`

