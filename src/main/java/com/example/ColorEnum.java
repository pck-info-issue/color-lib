package com.example;

import com.tmtron.enums.EnumMapper;

@EnumMapper
public enum ColorEnum {
    RED, BLUE, GREEN
}